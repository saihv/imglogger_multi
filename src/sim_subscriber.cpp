#include <message_filters/subscriber.h>
#include <image_transport/subscriber_filter.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <sensor_msgs/Image.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <string>
#include <termios.h>

using namespace sensor_msgs;
using namespace message_filters;

int imageNum;

void callback(const ImageConstPtr& leftimage, const ImageConstPtr& rightimage)
{
  ROS_INFO("Reached callback");
  cv_bridge::CvImagePtr ptrLeft, ptrRight;
  std::stringstream leftpath, rightpath;

  try
  {
    ptrLeft = cv_bridge::toCvCopy(leftimage, sensor_msgs::image_encodings::BGR8);
    ptrRight = cv_bridge::toCvCopy(rightimage, sensor_msgs::image_encodings::BGR8);
  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("cv_bridge exception: %s", e.what());
    return;
  }
  ROS_INFO("Saving images \n");

  leftpath << "/home/sai/left" <<imageNum << ".png";
  rightpath << "/home/sai/right" <<imageNum << ".png";
  cv::imwrite(leftpath.str(), ptrLeft->image);
  cv::imwrite(rightpath.str(), ptrRight->image);
  imageNum++;
}

int getch()
{
  static struct termios oldt, newt;
  tcgetattr( STDIN_FILENO, &oldt);           // save old settings
  newt = oldt;
  newt.c_lflag &= ~(ICANON);                 // disable buffering   
  newt.c_cc[VMIN] = 0; 
  newt.c_cc[VTIME] = 0;   
  tcsetattr( STDIN_FILENO, TCSANOW, &newt);  // apply new settings

  int c = getchar();  // read character (non-blocking)

  tcsetattr( STDIN_FILENO, TCSANOW, &oldt);  // restore old settings
  return c;
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "stereo_calibration");
  ros::NodeHandle nh;
  image_transport::ImageTransport it(nh);
  ros::Rate rate(100);
  
  typedef sync_policies::ApproximateTime<Image, Image> ApproxSyncPol;

  Subscriber<Image> leftimage_sub(nh, "pelican/front_cam/camera/image", 1);;
  Subscriber<Image> rightimage_sub(nh, "hummingbird/front_cam/camera/image", 1);

  Synchronizer<ApproxSyncPol> syncPol(ApproxSyncPol(10), leftimage_sub, rightimage_sub);

  syncPol.registerCallback(boost::bind(&callback, _1, _2));

  //Synchronizer<Image, Image> sync(leftimage_sub, rightimage_sub, 10);
  //sync.registerCallback(boost::bind(&callback, _1, _2));

  int key;

  while(ros::ok())
  {
    rate.sleep();
    key = getch();
    if (key == 32)
    {
      ROS_INFO("Key pressed \n");
      ros::spinOnce();
    }
    else
      ROS_DEBUG("Wrongkey pressed");

  }

  return 0;
}
